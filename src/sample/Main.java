package sample;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.*;
import javafx.scene.input.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Callback;

import java.io.*;
import java.net.MalformedURLException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Main extends Application {

    PaletteManager palette_manager;

    Stage main_window;

    Image chr_image;
    List<TileImageView> list_part_chr_imageviews;
    List<Meta2x2ImageView> list_2x2metatile_imageviews;
    int list_2x2metatile_imageview_index = 0;
    List<Meta4x4ImageView> list_4x4metatile_imageviews;
    int list_4x4metatile_imageview_index = 0;

    Scene scene_main;
    Button button_open_chr;
    Button button_screen_view_;

    Button button_open_2x2meta;
    Button button_save_2x2meta;

    Button button_open_4x4meta;
    Button button_save_4x4meta;

    ListView<MetaTile2x2> metatile2x2_listview;
    ListView<MetaTile4x4> metatile4x4_listview;

    boolean loaded_chr;
    boolean loaded_meta2x2;
    boolean loaded_meta4x4;
    boolean loaded_screens;

    ScreenWindow window_screen_;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception{

        loaded_chr = false;
        loaded_meta2x2 = false;
        loaded_meta4x4 = false;
        loaded_screens = false;

        main_window = primaryStage;
        VBox layout_main = new VBox(10);

        palette_manager = PaletteManager.getInstance(primaryStage);
        layout_main.getChildren().add(palette_manager.getManagerLayout());

        // Tiles, Metatiles, etc.
        HBox tile_main_layout = new HBox(1);

        // CHR
        VBox chr_main = addCHRBlockToWindow();

        // Metatiles
        HBox meta2x2_main = add2x2MetatileBlockToWindow();
        HBox meta4x4_main = add4x4MetatileBlockToWindow();

        // Add to needed layouts.
        tile_main_layout.getChildren().addAll(chr_main, meta2x2_main,meta4x4_main);
        layout_main.getChildren().add(tile_main_layout);


        scene_main = new Scene(layout_main,640,360);
        main_window.setTitle("Hello World");
        main_window.setScene(scene_main);
        main_window.setMinWidth(1280);
        main_window.setMinHeight(720);
        main_window.show();

    }

    public VBox addCHRBlockToWindow() {
        button_open_chr = new Button("Open CHR");
        button_open_chr.setOnAction(e -> openCHRFile());
        Button button_export_chr = new Button("Export CHR");
        button_export_chr.setDisable(true);
        button_screen_view_ = new Button("Screen View");
        button_screen_view_.setOnAction(e -> openScreenView());
        button_screen_view_.setDisable(true);
        list_part_chr_imageviews = new ArrayList<TileImageView>();
        HBox hbox_important_buttons = new HBox(1);
        hbox_important_buttons.getChildren().addAll(button_open_chr,button_export_chr,button_screen_view_);
        VBox chr_main = new VBox(10);
        chr_main.getChildren().addAll(hbox_important_buttons);
        VBox vlayout = new VBox(1);
        for(int y = 0; y < 16; ++y) {
            HBox hlayout = new HBox(1);
            for (int x = 0; x < 16; ++x) {
                WritableImage part_image = new WritableImage(16,16);
                TileImageView part_imageview = new TileImageView(part_image,x+16*y);
                part_imageview.setOnDragDetected(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        System.out.println("Drag detected");
                        Dragboard db = part_imageview.startDragAndDrop(TransferMode.ANY);
                        ClipboardContent content = new ClipboardContent();
                        content.putImage(part_imageview.getImage());
                        // TODO: add index of tile to content.
                        content.putString(part_imageview.getIndex().toString());
                        db.setContent(content);
                        System.out.println("Index of: " + db.getString() + " was placed in dragboard.");
                        event.consume();
                    }
                });
                list_part_chr_imageviews.add(part_imageview);
                hlayout.getChildren().add(part_imageview);
            }
            vlayout.getChildren().add(hlayout);
        }
        chr_main.getChildren().add(vlayout);
        return chr_main;
    }

    ImageView[] meta2x2_modifiable_view;
    Image[] meta2x2_tile_images;
    boolean[] meta2x2_tile_set;
    int[] meta2x2_tile_index;
    Button meta2x2_button_add;

    public HBox add2x2MetatileBlockToWindow() {
        // 2x2 metatiles
        VBox meta2x2_options = new VBox(1);
        meta2x2_options.setPrefWidth(70);

        button_open_2x2meta = new Button("Open 2x2");
        button_open_2x2meta.setMinWidth(meta2x2_options.getPrefWidth());
        button_open_2x2meta.setOnAction(e->open2x2MetaTiles());
        button_open_2x2meta.setDisable(true);
        button_save_2x2meta = new Button("Save 2x2");
        button_save_2x2meta.setMinWidth(meta2x2_options.getPrefWidth());
        button_save_2x2meta.setOnAction(e->save2x2MetaTiles());
        button_save_2x2meta.setDisable(true);

        meta2x2_button_add = new Button("Add");
        meta2x2_button_add.setMinWidth(meta2x2_options.getPrefWidth());
        meta2x2_button_add.setOnAction(e->addNew2x2MetaTile());
        meta2x2_button_add.setDisable(true);

        meta2x2_options.getChildren().addAll(button_open_2x2meta,button_save_2x2meta,meta2x2_button_add);
        meta2x2_modifiable_view = new ImageView[4];
        meta2x2_tile_images = new Image[4];
        meta2x2_tile_set = new boolean[4];
        meta2x2_tile_index = new int[4];

        VBox meta_modify_vlayout = new VBox(1);
        for(int y = 0; y < 2; ++y) {
            HBox meta_modify_hlayout = new HBox(1);
            for (int x = 0; x < 2; ++x) {
                HBox tempbox = new HBox(0);
                tempbox.setStyle("-fx-border-color: #CC33CC");
                WritableImage metatile_part = new WritableImage(32,32);
                TileImageView view_part = new TileImageView(metatile_part,x+16*y);
                meta2x2_modifiable_view[y*2 + x] = view_part;
                view_part.setPickOnBounds(true);
                tempbox.getChildren().add(view_part);
                tempbox.setOnDragEntered(new EventHandler<DragEvent>() {
                    @Override
                    public void handle(DragEvent event) {
                        System.out.println("Drag entered!");
                        event.consume();
                    }
                });
                tempbox.setOnDragOver(new EventHandler<DragEvent>() {
                    @Override
                    public void handle(DragEvent event) {
                        // TODO: check that it comes from CHRWindow
                        //if(event.getGestureSource() == )
                        if(event.getDragboard().hasImage()) {
                            event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
                        }
                        event.consume();
                    }
                });
                tempbox.setOnDragDropped(new EventHandler<DragEvent>() {
                    @Override
                    public void handle(DragEvent event) {
                        Dragboard db = event.getDragboard();
                        if(db.hasImage()) {
                            System.out.println("Image was dropped at: " + event.getTarget().getClass());
                            if(TileImageView.class.equals(event.getTarget().getClass())) {
                                System.out.println("Dropped over image!");
                                TileImageView my_obj = TileImageView.class.cast(event.getTarget());
                                my_obj.setImage(db.getImage());
                                my_obj.setFitHeight(32);
                                my_obj.setFitWidth(32);
                                System.out.println("Index :" + db.getString() + " was received.");
                                if(my_obj.equals(meta2x2_modifiable_view[0])) {
                                    meta2x2_tile_set[0] = true;
                                    meta2x2_tile_images[0] = db.getImage();
                                    meta2x2_tile_index[0] = Integer.parseInt(db.getString());
                                }
                                else if(my_obj.equals(meta2x2_modifiable_view[1])) {
                                    meta2x2_tile_set[2] = true;
                                    meta2x2_tile_images[2] = db.getImage();
                                    meta2x2_tile_index[2] = Integer.parseInt(db.getString());
                                }
                                else if(my_obj.equals(meta2x2_modifiable_view[2])) {
                                    meta2x2_tile_set[1] = true;
                                    meta2x2_tile_images[1] = db.getImage();
                                    meta2x2_tile_index[1] = Integer.parseInt(db.getString());
                                }
                                else if(my_obj.equals(meta2x2_modifiable_view[3])) {
                                    meta2x2_tile_set[3] = true;
                                    meta2x2_tile_images[3] = db.getImage();
                                    meta2x2_tile_index[3] = Integer.parseInt(db.getString());
                                }
                                else System.err.println("Should not happen!");
                            }
                            event.setDropCompleted(true);
                        }
                        else {
                            event.setDropCompleted(false);
                        }
                        event.consume();
                    }
                });

                meta_modify_hlayout.getChildren().add(tempbox);
            }
            meta_modify_vlayout.getChildren().add(meta_modify_hlayout);
        }

        meta2x2_options.getChildren().add(meta_modify_vlayout);

        HBox meta2x2_main = new HBox(1);
        meta2x2_main.setPrefWidth(600);
        metatile2x2_listview = new ListView<MetaTile2x2>();
        metatile2x2_listview.setEditable(true);
        metatile2x2_listview.setPrefWidth(meta2x2_main.getPrefWidth());
        metatile2x2_listview.setCellFactory(new Callback<ListView<MetaTile2x2>, ListCell<MetaTile2x2>>() {
            @Override
            public ListCell<MetaTile2x2> call(ListView<MetaTile2x2> param) {
                ListCell<MetaTile2x2> cell = new ListCell<MetaTile2x2>() {
                    @Override
                    protected void updateItem(MetaTile2x2 metatile, boolean empty) {
                        super.updateItem(metatile, empty);
                        if(metatile != null) {
                            setText(metatile.getIndexes());
                        }
                    }
                };
                return cell;
            }
        });

        list_2x2metatile_imageviews = new ArrayList<Meta2x2ImageView>();
        VBox vlayout = new VBox(1);
        for(int y = 0; y < 16; ++y) {
            HBox hlayout = new HBox(1);
            for (int x = 0; x < 16; ++x) {
                WritableImage part_image = new WritableImage(16,16);
                Meta2x2ImageView part_imageview = new Meta2x2ImageView(part_image,x+16*y);
                part_imageview.setOnDragDetected(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        System.out.println("Drag detected");
                        Dragboard db = part_imageview.startDragAndDrop(TransferMode.ANY);
                        ClipboardContent content = new ClipboardContent();
                        content.putImage(part_imageview.getImage());
                        // TODO: add index of tile to content.
                        content.putString(part_imageview.getIndex().toString());
                        db.setContent(content);
                        System.out.println("Index of: " + db.getString() + " was placed in dragboard.");
                        event.consume();
                    }
                });
                list_2x2metatile_imageviews.add(part_imageview);
                hlayout.getChildren().add(part_imageview);
            }
            vlayout.getChildren().add(hlayout);
        }
        vlayout.getChildren().add(metatile2x2_listview);

        meta2x2_main.getChildren().addAll(meta2x2_options,vlayout);
        return meta2x2_main;
    }

    public void open2x2MetaTiles() {
        if(loaded_chr) {
            System.out.println("Open 2x2 metatiles");
            FileChooser file_chooser = new FileChooser();
            file_chooser.setInitialDirectory(new File(PaletteColor.TEST_PATH));
            file_chooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("2x2 files","*.2x2"));
            file_chooser.setTitle("Select used 2x2 metatile file");
            File file = file_chooser.showOpenDialog(main_window);
            if(file != null) {
                Path path = Paths.get(file.getAbsolutePath());
                String filename = path.getFileName().toString();
                String file_extension = filename.substring(filename.lastIndexOf('.') + 1).toLowerCase();
                try {
                    InputStream is = new FileInputStream(file);
                    System.out.println("File size: " + file.length());
                    for (int i = 0; i < /*12; ++i) { //*/file.length() / 4; ++i) {
                        byte[] metatile = new byte[4];
                        is.read(metatile,0,4);
                        for (int j = 0; j < 4; ++j) {
                            int number = metatile[j];
                            number = number & 0xFF;
                            System.out.println("Index is: " + number);
                            System.out.println("i: " + i + ", j: " + j);

                            meta2x2_tile_images[j] = list_part_chr_imageviews.get(number).getImage();
                            System.out.println("2");
                            meta2x2_tile_set[j] = true;
                            System.out.println("3");
                            meta2x2_tile_index[j] = number;

                        }
                        System.out.println("Time to add metatile");
                        addNew2x2MetaTile();
                    }
                    loaded_meta2x2 = true;
                    button_open_4x4meta.setDisable(false);
                } catch (IOException ex) {
                    System.err.println("Could not read 2x2 metafile.");
                }
            }
        }
    }

    public void save2x2MetaTiles() {
        System.out.println("Save 2x2 metatiles");
        FileChooser file_chooser = new FileChooser();
        file_chooser.setInitialDirectory(new File(PaletteColor.TEST_PATH));
        file_chooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("2x2 file","*.2x2"));
        file_chooser.setTitle("Save 2x2 metatile");
        File file = file_chooser.showSaveDialog(main_window);

        if(file != null) {
            Path path = Paths.get(file.getAbsolutePath());
            String filename = path.getFileName().toString();
            String file_extension = filename.substring(filename.lastIndexOf('.') + 1).toLowerCase();


            byte[] meta2x2_bytes = new byte[metatile2x2_listview.getItems().size()*4];

            for (int i = 0; i < metatile2x2_listview.getItems().size(); ++i) {
                for(int j = 0; j < 4; ++j) {

                    if(j == 1) meta2x2_bytes[i*4+j] = metatile2x2_listview.getItems().get(i).getByteData(2);
                    else if(j == 2) meta2x2_bytes[i*4+j] = metatile2x2_listview.getItems().get(i).getByteData(1);
                    else meta2x2_bytes[i*4+j] = metatile2x2_listview.getItems().get(i).getByteData(j);
                    System.out.println(meta2x2_bytes[i]);
                }
            }

            try {
                FileOutputStream fstream = new FileOutputStream(file);
                try {
                    fstream.write(meta2x2_bytes);
                } finally {
                    fstream.close();
                }
            } catch(IOException ex) {
                System.err.println("Could not write 2x2 metatiles.");
            }
        }
    }

    public void addNew2x2MetaTile() {
        System.out.println("Add 2x2metatile.");
        boolean all_set = true;
        for(int i = 0; i < 4; ++i)
        {
            if(!meta2x2_tile_set[i]) {
                all_set = false;
                break;
            }
        }
        if(all_set) {
            WritableImage image = make2x2Image();

            // TODO: last parameter of constructor; palette_index
            MetaTile2x2 new_metatile2x2 = new MetaTile2x2(image,
                    meta2x2_tile_index[0],meta2x2_tile_index[2],
                    meta2x2_tile_index[1],meta2x2_tile_index[3]);
            metatile2x2_listview.getItems().add(new_metatile2x2);
            Meta2x2ImageView meta_imageview = list_2x2metatile_imageviews.get(list_2x2metatile_imageview_index);
            meta_imageview.setImage(image);
            meta_imageview.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    event.consume();
                    if(event.isPrimaryButtonDown()) {
                        System.out.println("Left click");
                    }
                    else if(event.isSecondaryButtonDown()) {
                        System.out.println("Right click");
                    }
                }
            });
            list_2x2metatile_imageview_index++;
            button_save_2x2meta.setDisable(false);
            meta4x4_button_add.setDisable(false);
        }
        else {
            System.out.println("Metatile is not ready!");
        }
    }

    public WritableImage make2x2Image() {
        WritableImage image  = new WritableImage(32,32);
        PixelWriter writer = image.getPixelWriter();
        for(int i = 0; i < 4; ++i) {
            PixelReader reader = meta2x2_tile_images[i].getPixelReader();
            for (int y = 0; y < meta2x2_tile_images[i].getHeight(); ++y) {
                for (int x = 0; x < meta2x2_tile_images[i].getWidth(); ++x) {
                    if (i == 0) writer.setColor(x, y, reader.getColor(x, y));//PaletteManager.getPaletteColor(,palette_index));
                    else if (i == 2) writer.setColor(16 + x, y, reader.getColor(x, y));//PaletteManager.getPaletteColor(reader.getColor(x, y),palette_index));
                    else if (i == 1) writer.setColor(x, 16 + y, reader.getColor(x, y));//PaletteManager.getPaletteColor(reader.getColor(x, y),palette_index));
                    else writer.setColor(16 + x, 16 + y, reader.getColor(x, y));//PaletteManager.getPaletteColor(reader.getColor(x, y),palette_index));
                }
            }
        }
        return image;
    }

    ImageView[] meta4x4_modifiable_view;
    Image[] meta4x4_tile_images;
    boolean[] meta4x4_tile_set;
    int[] meta4x4_tile_index;
    Button meta4x4_button_add;
    ComboBox<Integer>[] meta4x4_comboboxes_;

    final double META4x4_WIDTH = 134;

    public HBox add4x4MetatileBlockToWindow() {
        // 4x4 metatiles
        VBox meta4x4_options = new VBox(1);
        meta4x4_options.setPrefWidth(70);

        meta4x4_comboboxes_ = new ComboBox[4];

        for(int i = 0; i < 4; ++i) {
            meta4x4_comboboxes_[i] = new ComboBox<>();
            meta4x4_comboboxes_[i].getItems().addAll(0,1,2,3);
            meta4x4_comboboxes_[i].setValue(0);
            meta4x4_comboboxes_[i].setMinWidth(META4x4_WIDTH/2);
        }

        meta4x4_options.setPrefWidth(META4x4_WIDTH);

        button_open_4x4meta = new Button("Open 4x4");
        button_open_4x4meta.setMinWidth(meta4x4_options.getPrefWidth());
        button_open_4x4meta.setOnAction(e->open4x4MetaTiles());
        button_open_4x4meta.setDisable(true);



        button_save_4x4meta = new Button("Save 4x4");
        button_save_4x4meta.setMinWidth(meta4x4_options.getPrefWidth());
        button_save_4x4meta.setOnAction(e->save4x4MetaTiles());
        button_save_4x4meta.setDisable(true);

        meta4x4_button_add = new Button("Add");
        meta4x4_button_add.setMinWidth(meta4x4_options.getPrefWidth());
        meta4x4_button_add.setOnAction(e->addNew4x4MetaTile());
        meta4x4_button_add.setDisable(true);
        meta4x4_options.getChildren().addAll(button_open_4x4meta,button_save_4x4meta,meta4x4_button_add);
        meta4x4_modifiable_view = new ImageView[4];
        meta4x4_tile_images = new Image[4];
        meta4x4_tile_set = new boolean[4];
        meta4x4_tile_index = new int[4];

        VBox meta_modify_vlayout = new VBox(1);
        for(int y = 0; y < 2; ++y) {
            HBox meta_modify_hlayout = new HBox(1);
            for (int x = 0; x < 2; ++x) {
                HBox tempbox = new HBox(0);
                tempbox.setStyle("-fx-border-color: #CC33CC");
                WritableImage metatile_part = new WritableImage(64,64);
                Meta2x2ImageView view_part = new Meta2x2ImageView(metatile_part,x+16*y);
                meta4x4_modifiable_view[y*2 + x] = view_part;
                view_part.setPickOnBounds(true);
                tempbox.getChildren().add(view_part);
                tempbox.setOnDragEntered(new EventHandler<DragEvent>() {
                    @Override
                    public void handle(DragEvent event) {
                        System.out.println("Drag entered!");
                        event.consume();
                    }
                });
                tempbox.setOnDragOver(new EventHandler<DragEvent>() {
                    @Override
                    public void handle(DragEvent event) {
                        // TODO: check that it comes from CHRWindow
                        //if(event.getGestureSource() == )
                        if(event.getDragboard().hasImage()) {
                            event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
                        }
                        event.consume();
                    }
                });
                tempbox.setOnDragDropped(new EventHandler<DragEvent>() {
                    @Override
                    public void handle(DragEvent event) {
                        Dragboard db = event.getDragboard();
                        if(db.hasImage()) {
                            if(Meta2x2ImageView.class.equals(event.getTarget().getClass())) {
                                System.out.println("Dropped over image!");
                                Meta2x2ImageView my_obj = Meta2x2ImageView.class.cast(event.getTarget());
                                my_obj.setImage(db.getImage());
                                my_obj.setFitHeight(64);
                                my_obj.setFitWidth(64);
                                System.out.println("Index :" + db.getString() + " was received.");
                                if(my_obj.equals(meta4x4_modifiable_view[0])) {
                                    meta4x4_tile_set[0] = true;
                                    meta4x4_tile_images[0] = db.getImage();
                                    meta4x4_tile_index[0] = Integer.parseInt(db.getString());
                                }
                                else if(my_obj.equals(meta4x4_modifiable_view[1])) {
                                    meta4x4_tile_set[1] = true;
                                    meta4x4_tile_images[1] = db.getImage();
                                    meta4x4_tile_index[1] = Integer.parseInt(db.getString());
                                }
                                else if(my_obj.equals(meta4x4_modifiable_view[2])) {
                                    meta4x4_tile_set[2] = true;
                                    meta4x4_tile_images[2] = db.getImage();
                                    meta4x4_tile_index[2] = Integer.parseInt(db.getString());
                                }
                                else if(my_obj.equals(meta4x4_modifiable_view[3])) {
                                    meta4x4_tile_set[3] = true;
                                    meta4x4_tile_images[3] = db.getImage();
                                    meta4x4_tile_index[3] = Integer.parseInt(db.getString());
                                }
                                else System.err.println("Should not happen!");
                            }
                            event.setDropCompleted(true);
                        }
                        else {
                            event.setDropCompleted(false);
                        }
                        event.consume();
                    }
                });

                meta_modify_hlayout.getChildren().add(tempbox);
            }
            meta_modify_vlayout.getChildren().add(meta_modify_hlayout);
        }

        VBox meta4x4_opt_combo = new VBox(0);
        for(int y = 0; y < 2; ++y) {
            HBox meta4x4_opt_combo_tmp = new HBox(0);
            for(int x = 0; x < 2; ++x) {
                meta4x4_opt_combo_tmp.getChildren().add(meta4x4_comboboxes_[y*2+x]);
            }
            meta4x4_opt_combo.getChildren().add(meta4x4_opt_combo_tmp);
        }

        meta4x4_options.getChildren().addAll(meta_modify_vlayout,meta4x4_opt_combo);




        HBox meta4x4_main = new HBox(1);
        meta4x4_main.setPrefWidth(600);
        metatile4x4_listview = new ListView<MetaTile4x4>();
        metatile4x4_listview.setPrefWidth(meta4x4_main.getPrefWidth());
        metatile4x4_listview.setCellFactory(new Callback<ListView<MetaTile4x4>, ListCell<MetaTile4x4>>() {
            @Override
            public ListCell<MetaTile4x4> call(ListView<MetaTile4x4> param) {
                ListCell<MetaTile4x4> cell = new ListCell<MetaTile4x4>() {
                    @Override
                    protected void updateItem(MetaTile4x4 metatile, boolean empty) {
                        super.updateItem(metatile, empty);
                        if(metatile != null) {
                            setText(metatile.getIndexes());
                        }
                    }
                };
                return cell;
            }
        });
        list_4x4metatile_imageviews = new ArrayList<Meta4x4ImageView>();
        VBox vlayout = new VBox(1);
        for(int y = 0; y < 16; ++y) {
            HBox hlayout = new HBox(1);
            for (int x = 0; x < 16; ++x) {
                WritableImage part_image = new WritableImage(64,64);
                Meta4x4ImageView part_imageview = new Meta4x4ImageView(part_image,x+16*y);
                part_imageview.setOnDragDetected(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        System.out.println("Drag detected");
                        Dragboard db = part_imageview.startDragAndDrop(TransferMode.ANY);
                        ClipboardContent content = new ClipboardContent();
                        content.putImage(part_imageview.getImage());
                        content.putString(part_imageview.getIndex().toString());
                        // TODO: add index of tile to content.
                        db.setContent(content);
                        event.consume();
                    }
                });
                list_4x4metatile_imageviews.add(part_imageview);
                hlayout.getChildren().add(part_imageview);
            }
            vlayout.getChildren().add(hlayout);
        }
        vlayout.getChildren().add(metatile4x4_listview);

        meta4x4_main.getChildren().addAll(meta4x4_options,vlayout);
        return meta4x4_main;
    }

    public void open4x4MetaTiles() {
        System.out.println("Open 4x4 metatiles");
        FileChooser file_chooser = new FileChooser();
        file_chooser.setInitialDirectory(new File(PaletteColor.TEST_PATH));
        file_chooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("4x4 files","*.4x4"));
        file_chooser.setTitle("Select used 4x4 metatile file");
        File file = file_chooser.showOpenDialog(main_window);
        File file_attr = new File(file.getAbsolutePath()+"_attributes");
        if(file != null && file_attr != null) {
            Path path = Paths.get(file.getAbsolutePath());
            String filename = path.getFileName().toString();
            String file_extension = filename.substring(filename.lastIndexOf('.') + 1).toLowerCase();

            try {
                InputStream is = new FileInputStream(file);
                InputStream is2 = new FileInputStream(file_attr);
                System.out.println("File size: " + file.length());
                for (int i = 0; i < file.length() / 4; ++i) {
                    byte[] metatile = new byte[4];
                    is.read(metatile,0,4);
                    for (int j = 0; j < 4; ++j) {
                        int number = metatile[j];
                        number = number & 0xFF;
                        meta4x4_tile_images[j] = list_2x2metatile_imageviews.get(number).getImage();
                        meta4x4_tile_set[j] = true;
                        meta4x4_tile_index[j] = number;
                    }
                    byte[] meta_attr = new byte[1];
                    is2.read(meta_attr,0,1);
                    meta4x4attrToCombobox(meta_attr[0]);
                    addNew4x4MetaTile();
                }
                loaded_meta4x4 = true;
            } catch (IOException ex) {
                System.err.println("Could not read 4x4 metafile.");
            }
        }
    }

    public void save4x4MetaTiles() {
        System.out.println("Save 4x4 metatiles");
        FileChooser file_chooser = new FileChooser();
        file_chooser.setInitialDirectory(new File(PaletteColor.TEST_PATH));
        file_chooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("4x4 file","*.4x4"));
        file_chooser.setTitle("Save 4x4 metatile");
        File file = file_chooser.showSaveDialog(main_window);

        if(file != null) {
            Path path = Paths.get(file.getAbsolutePath());
            String filename = path.getFileName().toString();
            String file_extension = filename.substring(filename.lastIndexOf('.') + 1).toLowerCase();
            File file2 = new File(file.getAbsolutePath() + "_attributes");

            byte[] meta4x4_bytes = new byte[metatile4x4_listview.getItems().size()*4];

            byte[] attribute_bytes = new byte[256];

            for (int i = 0; i < metatile4x4_listview.getItems().size(); ++i) {
                for(int j = 0; j < 4; ++j) {
                    meta4x4_bytes[i*4+j] = metatile4x4_listview.getItems().get(i).getByteData(j);
                    //System.out.println(meta4x4_bytes[i]);
                }
                attribute_bytes[i] = metatile4x4_listview.getItems().get(i).getByteData(4);
            }

            try {
                FileOutputStream fstream = new FileOutputStream(file);
                fstream.write(meta4x4_bytes);
                fstream.close();
                fstream = new FileOutputStream(file2);
                fstream.write(attribute_bytes);
                fstream.close();
            } catch(IOException ex) {
                System.err.println("Could not write 4x4 metatiles.");
            }
        }
    }

    public void addNew4x4MetaTile() {
        System.out.println("Add 4x4metatile.");
        boolean all_set = true;
        for(int i = 0; i < 4; ++i)
        {
            if(!meta4x4_tile_set[i]) {
                all_set = false;
                break;
            }
        }
        if(all_set) {
            WritableImage image = make4x4Image();
            MetaTile4x4 new_metatile4x4 = new MetaTile4x4(image,
                    meta4x4_tile_index[0],meta4x4_tile_index[1],meta4x4_tile_index[2],meta4x4_tile_index[3],meta4x4attributeValue());
            metatile4x4_listview.getItems().add(new_metatile4x4);
            list_4x4metatile_imageviews.get(list_4x4metatile_imageview_index).setImage(image);
            list_4x4metatile_imageviews.get(list_4x4metatile_imageview_index).setFitWidth(32);
            list_4x4metatile_imageviews.get(list_4x4metatile_imageview_index).setFitHeight(32);
            list_4x4metatile_imageview_index++;
            button_screen_view_.setDisable(false);
            button_save_4x4meta.setDisable(false);
        }
        else {
            System.out.println("Metatile is not ready!");
        }
    }

    public WritableImage make4x4Image() {
        WritableImage image  = new WritableImage(64,64);
        PixelWriter writer = image.getPixelWriter();
        for(int i = 0; i < 4; ++i) {
            PixelReader reader = meta4x4_tile_images[i].getPixelReader();
            for (int y = 0; y < meta4x4_tile_images[i].getHeight(); ++y) {
                for (int x = 0; x < meta4x4_tile_images[i].getWidth(); ++x) {
                    if (i == 0) writer.setColor(x, y, PaletteManager.getPaletteColor(reader.getColor(x, y),meta4x4_comboboxes_[i].getValue()));//reader.getColor(x, y));
                    else if (i == 1) writer.setColor(32 + x, y, PaletteManager.getPaletteColor(reader.getColor(x, y),meta4x4_comboboxes_[i].getValue()));//reader.getColor(x, y));
                    else if (i == 2) writer.setColor(x, 32 + y, PaletteManager.getPaletteColor(reader.getColor(x, y),meta4x4_comboboxes_[i].getValue()));//reader.getColor(x, y));
                    else writer.setColor(32 + x, 32 + y, PaletteManager.getPaletteColor(reader.getColor(x, y),meta4x4_comboboxes_[i].getValue()));//reader.getColor(x, y));
                }
            }
        }
        return image;
    }

    public void meta4x4attrToCombobox(byte attribute_value) {
        int temp = attribute_value;
        for( int i = 0; i < 4; ++i) {
            meta4x4_comboboxes_[i].setValue(temp & 0x03);
            temp = (temp >> 2);
        }
    }

    public byte meta4x4attributeValue() {
        byte attribute = 0x00;
        for(int i = 0; i < 4; ++i) {
            attribute += (meta4x4_comboboxes_[i].getValue() << i*2);
        }

        return attribute;
    }

    public void openCHRFile() {
        FileChooser file_chooser = new FileChooser();
        file_chooser.setInitialDirectory(new File(PaletteColor.TEST_PATH));
        file_chooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("IMG files","*.png", "*.bmp"));
        file_chooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("CHR files (*.chr)","*.chr"));
        file_chooser.setTitle("Select used CHR-file");
        File file = file_chooser.showOpenDialog(main_window);
        if(file != null) {
            Path path = Paths.get(file.getAbsolutePath());
            String filename = path.getFileName().toString();
            String file_extension = filename.substring(filename.lastIndexOf('.')+1).toLowerCase();
            main_window.setTitle(filename);

            if(file_extension == "chr") {
                //TODO: create image from chr data.
                chr_image = new WritableImage(256,256);
            }
            else {
                try {
                    chr_image = new Image(file.toURI().toURL().toString(), 256, 256, false, false);
                    System.out.println("Success!");
                } catch (MalformedURLException e) {
                    System.err.println("Could not open png/bmp image.");
                    chr_image = new WritableImage(256, 256);
                }
                /*// Set palette based on image colors
                for(int i = 0; i < 4; ++i) {
                    Color temp = chr_image.getPixelReader().getColor(1+16*i,1);
                    palette_manager.setPaletteValueTo(
                            PaletteColor.getPaletteIndex("#" + temp.toString().substring(2,8)),
                            i);
                }*/
            }

            PixelReader reader = chr_image.getPixelReader();
            for(int y = 0; y < 16; ++y) {
                for(int x = 0; x < 16; ++x) {
                    WritableImage part_image = new WritableImage(reader,x*16,y*16,16,16);
                    list_part_chr_imageviews.get(x+y*16).setImage(part_image);
                }
            }
            loaded_chr = true;
            button_open_2x2meta.setDisable(false);
            meta2x2_button_add.setDisable(false);
        }
    }

    public void openScreenView() {
        ScreenInitWindow screen_init_window = new ScreenInitWindow();
        Optional<ButtonType> result = screen_init_window.showAndWait();
        if(result.isPresent() && result.get().equals(ButtonType.OK)) {
            System.out.println("Creating new screen");
            window_screen_ = new ScreenWindow(
                    screen_init_window.getScreenW(),
                    screen_init_window.getScreenH(),
                    screen_init_window.getTileSize(),
                    metatile2x2_listview, metatile4x4_listview
            );
            window_screen_.show();
        }
    }
}
