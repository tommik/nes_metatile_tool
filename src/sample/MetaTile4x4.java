package sample;

import javafx.scene.image.Image;

/**
 * Created by Tommi on 14.4.2016.
 */
public class MetaTile4x4 extends MetaTile {

    byte attribute_value_;

    MetaTile4x4(Image image, int index0, int index1, int index2, int index3, byte attribute_value) {
        super(image,index0,index1,index2,index3);
        attribute_value_ = attribute_value;
    }

    public byte getAttributeValue() {
        return attribute_value_;
    }

    @Override
    public byte getByteData(int index) {
        if (4 > index && -1 < index) {
            int temp_int = block_index_[index];
            byte rvalue = (byte) (temp_int);
            return rvalue;
        }
        else if(index == 4) {
            return attribute_value_;
        }
        return 0;
    }

}
