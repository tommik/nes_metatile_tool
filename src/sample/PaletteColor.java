package sample;

import javafx.scene.paint.Color;

/**
 * Created by Tommi on 8.4.2016.
 */
public class PaletteColor {
    //final static String TEST_PATH = "T:/BitBucket/assembly/kraigan/game/levels/space";
    final static String TEST_PATH = "T:/BitBucket/nes/assembly/projectpuzzlehero/world/forest";

    final static String[] PALETTE_STRING_TABLE = {
            "#7C7C7C",      // originally: 7C7C7C
            "#0000FC",
            "#0000BC",
            "#4428BC",
            "#940084",
            "#A80020",
            "#A81000",
            "#881400",
            "#503000",
            "#007800",
            "#006800",
            "#005800",
            "#004058",
            "#000000",
            "#000000",
            "#000000",
            "#BCBCBC",      // originally: BCBCBC
            "#0078F8",
            "#0058F8",
            "#6844FC",
            "#D800CC",
            "#E40058",
            "#F83800",
            "#E45C10",
            "#AC7C00",
            "#00B800",
            "#00A800",
            "#00A844",
            "#008888",
            "#000000",
            "#000000",
            "#000000",
            "#F8F8F8",      // originally: F8F8F8
            "#3CBCFC",
            "#6888FC",
            "#9878F8",
            "#F878F8",
            "#F85898",
            "#F87858",
            "#FCA044",
            "#F8B800",
            "#B8F818",
            "#58D854",
            "#58F898",
            "#00E8D8",
            "#787878",
            "#000000",
            "#000000",
            "#FCFCFC",      // originally: FCFCFC
            "#A4E4FC",
            "#B8B8F8",
            "#D8B8F8",
            "#F8B8F8",
            "#F8A4C0",
            "#F0D0B0",
            "#FCE0A8",
            "#F8D878",
            "#D8F878",
            "#B8F8B8",
            "#B8F8D8",
            "#00FCFC",
            "#F8D8F8",
            "#000000",
            "#000000"
    };

    public static Integer getPaletteIndex(String color) {
        for( Integer i = 0; i < PALETTE_STRING_TABLE.length; ++i) {
            if(PALETTE_STRING_TABLE[i].toLowerCase().equals(color)) {
                System.out.println("Modifying palette with: " + i.toString() );
                return i;
            }
        }
        return 0;
    }
}
