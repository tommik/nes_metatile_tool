package sample;

import javafx.scene.control.TextField;

/**
 * Created by Tommi on 8.4.2016.
 */
public class PaletteTextField extends TextField {

    PaletteManager my_manager;
    Boolean is_background;
    byte palette_number_;

    public PaletteTextField(PaletteManager manager,String text, Boolean is_bg) {
        super(text);

        my_manager = manager;
        is_background = is_bg;
        palette_number_ = 13;
        setBGColor(13); // $0D = 13
    }

    @Override
    public void replaceText(int start, int end, String text) {
        super.replaceText(start, end, text);
        verifyText();
    }

    @Override
    public void replaceSelection(String text) {
        super.replaceSelection(text);
        verifyText();
    }

    public void verifyText(){
        if(!getText().matches("^[a-fA-F0-9]+$")) {
            setText("");
        }
        if(getText().length() > 2) {
            setText(getText().substring(0,2));
        }
        if(getText().length() > 1) {
            int palette_number = Integer.parseInt(getText(),16);
            while(palette_number > 63) {
                palette_number = palette_number - 64;
            }
            setText(String.format("%02X",palette_number));
            System.out.println("New palette number is: " + palette_number);
            palette_number_ = (byte)palette_number;
            setBGColor(palette_number);
            if(is_background) {
                System.out.println("Test");
                my_manager.paletteBGSetTo(palette_number);
            }
        }
    }

    public byte getColorIndex() {
        return palette_number_;
    }

    public void setBGColor(int palette_number) {
        palette_number_ = (byte)palette_number;
        if(is_background) {
            setStyle("-fx-control-inner-background: " +
                    PaletteColor.PALETTE_STRING_TABLE[palette_number] +
                    "; -fx-text-box-border: red; -fx-focus-color: red;");
        }
        else {
            setStyle("-fx-control-inner-background: " + PaletteColor.PALETTE_STRING_TABLE[palette_number] + ";");
        }
    }
}
