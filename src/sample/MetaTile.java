package sample;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * Created by Tommi on 14.4.2016.
 */
public class MetaTile {

    protected ImageView imageview_metatile_;
    protected Integer[] block_index_;

    MetaTile(Image image, int index0, int index1, int index2, int index3) {
        imageview_metatile_ = new ImageView(image);
        block_index_ = new Integer[4];
        block_index_[0] = index0;
        block_index_[1] = index1;
        block_index_[2] = index2;
        block_index_[3] = index3;
    }

    public String getIndexes()
    {
        String value = new String();
        for(int i = 0; i < 4; ++i) {
            value += block_index_[i].toString();
            if(i != 3) {
                value += ", ";
            }
        }
        return value;
    }

    public Image getImage() {
        return imageview_metatile_.getImage();
    }

    public byte getByteData(int index) {
        if (4 > index && -1 < index) {
            int temp_int = block_index_[index];
            byte rvalue = (byte) (temp_int);
            return rvalue;
        }
        return 0;
    }
}
