package sample;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * Created by Tommi on 14.4.2016.
 */
public class Meta2x2ImageView extends ImageView {

    private Integer index_;

    Meta2x2ImageView(Image image, Integer index) {
        super(image);
        index_ = index;
    }

    public Integer getIndex() {
        return index_;
    }
}
