package sample;

import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.paint.Color;

/**
 * Created by Tommi on 14.4.2016.
 */
public class Tile {

    final static String DEFAULT_TILE_COLORS[] = {
            "000000",            // $0D
            "444444",            // $00
            "888888",            // $10
            "CCCCCC"             // $20
    };

    byte[][] tile_color_;
    byte[] tile_chr_data_;

    Tile(Image image) {
        tile_color_ = new byte[8][8];
        tile_chr_data_ = new byte[16];

        PixelReader reader = image.getPixelReader();
        for(int y= 0; y < 8; ++y) {
            for (int x = 0; x < 8; ++x) {
                for (int index = 0; index < DEFAULT_TILE_COLORS.length; ++index) {
                    if (reader.getColor(x, y).toString().equals(DEFAULT_TILE_COLORS[index])) {
                        tile_color_[x][y] = (byte)index;
                        break;
                    }
                }
            }
        }

        for(byte y= 0; y < 8; ++y) {
            tile_chr_data_[y] = (byte)(0x05 << y);
            for (int x = 0; x < 8; ++x) {
                if(tile_color_[x][y] == 0) tile_chr_data_[y] &= ~(1<<x);
                else if(tile_color_[x][y] == 1) tile_chr_data_[y] |= (1<<x);
                else if(tile_color_[x][y] == 2) tile_chr_data_[8+y] &= ~(1<<x);
                else if(tile_color_[x][y] == 3) tile_chr_data_[8+y] |= (1<<x);
            }
        }
    }

    public void colorTile(Image image) {
    }
}
