package sample;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static sample.PaletteColor.PALETTE_STRING_TABLE;

/**
 * Created by Tommi on 8.4.2016.
 */
public class PaletteManager {


    private static Stage my_stage;
    private VBox layout_main;
    private Button button_import_palette;
    private Button button_export_palette;

    private static List<PaletteTextField> list_palette_colors;

    private static PaletteManager palette_manager_ = new PaletteManager();

    private PaletteManager() {

        layout_main = new VBox(10);

        HBox hlayout_palette_header = new HBox(2);
        button_import_palette = new Button("Import palette");
        button_import_palette.setOnAction(e->importPalette());
        button_export_palette = new Button("Export palette");
        button_export_palette.setOnAction(e->exportPalette());
        hlayout_palette_header.getChildren().add(button_import_palette);
        hlayout_palette_header.getChildren().add(button_export_palette);
        //hlayout_palette_header.setAlignment(Pos.CENTER);

        layout_main.getChildren().add(hlayout_palette_header);

        HBox hlayout_palette_data = new HBox(2);
        list_palette_colors = initPaletteSection(hlayout_palette_data);

        layout_main.getChildren().add(hlayout_palette_data);

    }


    // #000000
    // #666666
    // #AAAAAA
    // #FFFFFF

    public static Color getPaletteColor(Color base_color, int palette_set) {
        int palette_index = 4*palette_set;
        switch(base_color.toString().substring(2,8).toLowerCase()) {
            case "ffffff":
                palette_index++;
            case "aaaaaa":
                palette_index++;
            case "666666":
                palette_index++;
            case "000000":
                break;
            default:
                System.err.println("Tile contains non traditional color!");
                return new Color(255,0,255,0);
        }
        return Color.valueOf(PALETTE_STRING_TABLE[list_palette_colors.get(palette_index).getColorIndex()]);
    }

    public static PaletteManager getInstance(Stage stage)  {
        my_stage = stage;
        return palette_manager_;
    }

    public VBox getManagerLayout()
    {
        return layout_main;
    }

    private List<PaletteTextField> initPaletteSection(HBox layout_horizontal) {

        List<PaletteTextField> list_palette_fields = new ArrayList<PaletteTextField>();
        for(int i = 0; i < 16; ++i) {
            PaletteTextField newTextField;
            if(i%4 == 0) {
                newTextField = new PaletteTextField(this,"0D",true);
            }
            else {
                newTextField = new PaletteTextField(this,"0D",false);
            }
            newTextField.focusedProperty().addListener((observable,oldb,newb) -> focusChanged(newTextField,oldb,newb));
            list_palette_fields.add(newTextField);
            layout_horizontal.getChildren().add(newTextField);
        }
        return list_palette_fields;
    }

    public void paletteBGSetTo(int palette_number)
    {
        for(int i = 0; i < 13; ++i)
        {
            if(i%4 == 0) {
                list_palette_colors.get(i).setText(String.format("%02X",palette_number));
                list_palette_colors.get(i).setBGColor(palette_number);
            }
        }
    }

    public void setPaletteValueTo(int palette_number, int index) {
        System.out.println("Modifying palette:" + palette_number + ", " + index);
        if(index%4 == 0) {
            paletteBGSetTo(palette_number);
        }
        else {
            list_palette_colors.get(index).setText(String.format("%02X",palette_number));
            list_palette_colors.get(index).setBGColor(palette_number);
        }
    }

    public Image color2x2Metatile(Image image, int palette_index) {
        //for(int i = 0; i < )
        return image;
    }

    private void focusChanged(PaletteTextField field, Boolean oldb, Boolean newb) {
        if(!newb && field.getText().length() < 2) {
            field.setText("0D");
            field.verifyText();
        }
    }

    private void importPalette() {
        System.out.println("Import palette");
        FileChooser file_chooser = new FileChooser();
        file_chooser.setInitialDirectory(new File(PaletteColor.TEST_PATH));
        file_chooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Palette files","*.pal"));
        file_chooser.setTitle("Import palette");
        File file = file_chooser.showOpenDialog(my_stage);
        if(file != null) {
            Path path = Paths.get(file.getAbsolutePath());
            String filename = path.getFileName().toString();
            String file_extension = filename.substring(filename.lastIndexOf('.') + 1).toLowerCase();
            try {
                InputStream is = new FileInputStream(file);
                for (int i = 0; i < 16; ++i) {
                    byte[] data = new byte[1];
                    is.read(data,0,1);
                    setPaletteValueTo((int)data[0],i);
                }
            } catch (IOException ex) {
                System.err.println("Could not read 2x2 metafile.");
            }
        }
    }

    private void exportPalette() {
        System.out.println("Export palette");
        FileChooser file_chooser = new FileChooser();
        file_chooser.setInitialDirectory(new File(PaletteColor.TEST_PATH));
        file_chooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Palette file","*.pal"));
        file_chooser.setTitle("Export palette");
        File file = file_chooser.showSaveDialog(my_stage);

        if(file != null) {
            Path path = Paths.get(file.getAbsolutePath());
            String filename = path.getFileName().toString();
            String file_extension = filename.substring(filename.lastIndexOf('.') + 1).toLowerCase();

            byte[] palette_bytes = new byte[16];

            for (int i = 0; i < list_palette_colors.size(); ++i) {
                palette_bytes[i] = list_palette_colors.get(i).getColorIndex();
            }

            try {
                FileOutputStream fstream = new FileOutputStream(file);
                try {
                    fstream.write(palette_bytes);
                } finally {
                    fstream.close();
                }
            } catch(IOException ex) {
                System.err.println("Could not write palette data.");
            }
        }
    }
}
