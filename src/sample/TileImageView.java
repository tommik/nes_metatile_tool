package sample;

import javafx.event.EventHandler;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelReader;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.paint.Color;

/**
 * Created by Tommi on 14.4.2016.
 */

public class TileImageView extends ImageView {

    private Integer index_;
    byte[] tile_data_;

    TileImageView(Image image, Integer index) {
        super(image);
        index_ = index;
        tile_data_ = new byte[16];
    }

    public Integer getIndex() {
        return index_;
    }
}
