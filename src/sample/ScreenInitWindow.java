package sample;

import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import javafx.stage.Stage;

/**
 * Created by Tommi on 19.4.2016.
 */
public class ScreenInitWindow extends Dialog {

    ComboBox<String> tilesizes_;
    ComboBox<String> widths_;
    ComboBox<String> heights_;

    ButtonType btn_ok_;
    boolean ok_pressed_ = false;

    ScreenInitWindow() {
        setTitle("Screen settings");
        VBox main_layout = new VBox(4);
        Label label0 = new Label("Used tilesize:");
        tilesizes_ = new ComboBox<>();
        tilesizes_.setPrefWidth(300);
        tilesizes_.getItems().addAll("CHR", "2x2", "4x4");
        tilesizes_.setOnAction(e-> tilesizeChanged());

        Label label1 = new Label("Width:");
        widths_ = new ComboBox<>();
        widths_.setPrefWidth(300);
        widths_.setDisable(true);
        Label label2 = new Label("Height:");
        heights_ = new ComboBox<>();
        heights_.setDisable(true);
        heights_.setPrefWidth(300);

        main_layout.getChildren().addAll(label0,tilesizes_,label1,widths_,label2,heights_);
        //setScene(scene);

        getDialogPane().getButtonTypes().addAll(ButtonType.OK,ButtonType.CANCEL);
        getDialogPane().lookupButton(ButtonType.OK).setDisable(true);
        getDialogPane().setContent(main_layout);
        //System.out.println("km\u00B2");
    }

    private void tilesizeChanged() {
        String selected = tilesizes_.getSelectionModel().getSelectedItem().toString();
        widths_.setDisable(false);
        heights_.setDisable(false);
        widths_.getItems().clear();
        heights_.getItems().clear();
        widths_.setValue("256");
        heights_.setValue("224");
        getDialogPane().lookupButton(ButtonType.OK).setDisable(false);

        if("CHR".equals(selected)) {
            for(Integer i = 256; i > 0; i-=8) {
                widths_.getItems().add(i.toString());
                if(i <= 240) heights_.getItems().add(i.toString());
            }
            System.out.println("CHR was selected");
        }
        else if("2x2".equals(selected)) {
            for(Integer i = 256; i > 0; i-=16) {
                widths_.getItems().add(i.toString());
                if(i <= 240) heights_.getItems().add(i.toString());
            }
            System.out.println("2x2 was selected");
        }
        else if("4x4".equals(selected)) {
            for(Integer i = 256; i > 0; i-=32) {
                widths_.getItems().add(i.toString());
                if(i <= 240) heights_.getItems().add(i.toString());
            }
            System.out.println("4x4 was selected");
        }
        else {
            widths_.setDisable(true);
            heights_.setDisable(true);
        }
    }

    private void btnOkPressed()
    {
        ok_pressed_ = true;
        close();
    }

    public int getScreenW() {
        return Integer.parseInt(widths_.getSelectionModel().getSelectedItem().toString());
    }

    public int getScreenH() {
        return Integer.parseInt(heights_.getSelectionModel().getSelectedItem().toString());
    }

    public int getTileSize() {
        if("CHR".equals(tilesizes_.getSelectionModel().getSelectedItem().toString())) return 8;
        else if("2x2".equals(tilesizes_.getSelectionModel().getSelectedItem().toString())) return 16;
        else if("4x4".equals(tilesizes_.getSelectionModel().getSelectedItem().toString())) return 32;
        else return 2;
    }
}
