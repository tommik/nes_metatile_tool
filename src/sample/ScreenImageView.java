package sample;

import javafx.scene.image.*;
import javafx.scene.paint.Color;

import javax.imageio.ImageWriter;

/**
 * Created by Tommi on 19.4.2016.
 */
public class ScreenImageView extends ImageView {

    private Integer index_;
    private Integer collision_;
    Image image_;

    ScreenImageView(Image image, Integer index, Integer collision) {
        super(image);
        index_ = index;
        collision_ = collision;
    }

    public void setIndex(int index) {
        index_ = index;
    }

    public Integer getIndex() {
        return index_;
    }

    public void setCollision(int collision) { collision_ = collision;}

    public void increaseCollision() {
        collision_++;
        if(collision_ > 15) collision_ = 0;
        System.out.println("Collision is now: " + collision_.toString());
        drawCollision();
    }

    public void decreaseCollision() {
        collision_--;
        if(collision_ < 0) collision_ = 15;
        System.out.println("Collision is now: " + collision_.toString());
        drawCollision();
    }

    public Integer getCollision() {return collision_;}

    public void switchToCollisionMode() {
        image_ = getImage();
    }

    public void drawCollision() {
        int width = (int)image_.getWidth();
        int height = (int)image_.getHeight();
        WritableImage coll_image = new WritableImage(width,height);
        PixelWriter writer = coll_image.getPixelWriter();
        writer.setPixels(0,0,width,height,image_.getPixelReader(),0,0);
        WritableImage white_image = new WritableImage(width,height);
        for(int y = 0; y < height; ++y) for(int x = 0; x < width; ++x) white_image.getPixelWriter().setColor(x,y, Color.WHITE);

        if((collision_&0x01)>0) writer.setPixels(width/2,0,width/2,height/2,white_image.getPixelReader(),0,0);
        if((collision_&0x02)>0) writer.setPixels(0,0,width/2,height/2,white_image.getPixelReader(),0,0);
        if((collision_&0x04)>0) writer.setPixels(width/2,height/2,width/2,height/2,white_image.getPixelReader(),0,0);
        if((collision_&0x08)>0) writer.setPixels(0,height/2,width/2,height/2,white_image.getPixelReader(),0,0);
        setImage(coll_image);
    }

    public void unDrawCollision() {
        setImage(image_);
    }
}
