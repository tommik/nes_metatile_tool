package sample;

import javafx.scene.control.ListCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelReader;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

/**
 * Created by Tommi on 12.4.2016.
 */
public class MetaTile2x2 extends MetaTile {

    private Integer palette_index_;

    MetaTile2x2(Image image, int index0, int index1, int index2, int index3) {
        super(image,index0,index1,index2,index3);
        palette_index_ = 0;
    }

    public void setPaletteIndex(int palette_index) {
        palette_index_ = palette_index;
    }

    public Integer getPaletteIndex() {
        return palette_index_;
    }

    public byte getByteData(int index) {
        if (4 > index && -1 < index) {
            int temp_int = block_index_[index];
            byte rvalue = (byte) (temp_int);
            return rvalue;
        }
        return 0;
    }

}
