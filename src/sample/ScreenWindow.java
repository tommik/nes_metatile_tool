package sample;

import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by Tommi on 19.4.2016.
 */
public class ScreenWindow extends Stage {

    ListView<MetaTile2x2> metatile2x2_listview_;
    ListView<MetaTile4x4> metatile4x4_listview_;
    VBox screen_v_layout;
    ComboBox<String> scroll_mode_;
    int screen_width_;
    int screen_height_;
    int tilesize_;
    boolean collision_mode_;

    String screenFilename;

    List<ScreenImageView> screen_imageviews_;

    ScreenWindow(
            int width, int height, int tilesize,
            ListView<MetaTile2x2> metatile2x2_listview,
            ListView<MetaTile4x4> metatile4x4_listview) {

        setAlwaysOnTop(true);
        screenFilename = "";
        screen_width_ = width;
        screen_height_ = height;
        tilesize_ = tilesize;
        screen_imageviews_ = new ArrayList<>();
        metatile2x2_listview_ = metatile2x2_listview;
        metatile4x4_listview_ = metatile4x4_listview;
        collision_mode_ = false;
        build(width,height,tilesize);
    }

    private void newScreen() {
        ScreenInitWindow screen_init_window = new ScreenInitWindow();
        Optional<ButtonType> result = screen_init_window.showAndWait();
        if(result.isPresent() && result.get().equals(ButtonType.OK)) {
            System.out.println("Creating new screen");
            rebuild(screen_init_window.getScreenW(),screen_init_window.getScreenH(),screen_init_window.getTileSize());
        }
    }

    private void rebuild(int width, int height, int tilesize) {
        build(width,height,tilesize);
    }

    private void build(int width, int height, int tilesize) {
        setTitle("Screen creation");
        System.out.println("Screen W: " + width/tilesize);
        System.out.println("Screen H: " + height/tilesize);
        screen_v_layout = new VBox(1);
        for(int y = 0; y < height/tilesize; ++y) {
            HBox screen_h_layout = new HBox(1);
            for(int x = 0; x < width/tilesize; ++x) {
                HBox tempbox = new HBox(0);
                tempbox.setStyle("-fx-border-color: #CC33CC");
                WritableImage screen_part = new WritableImage(tilesize*2,tilesize*2);
                ScreenImageView image_view = new ScreenImageView(screen_part,y*(width/tilesize)+x,0);
                screen_imageviews_.add(image_view);
                image_view.setPickOnBounds(true);
                tempbox.getChildren().add(image_view);
                tempbox.setOnDragOver(new EventHandler<DragEvent>() {
                    @Override
                    public void handle(DragEvent event) {
                        // TODO: check that it comes from 4x4Metatile list
                        //if(event.getGestureSource() == )
                        if(event.getDragboard().hasImage()) {
                            event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
                        }
                        event.consume();
                    }
                });
                tempbox.setOnDragDropped(new EventHandler<DragEvent>() {
                    @Override
                    public void handle
                    (DragEvent event) {
                        Dragboard db = event.getDragboard();
                        System.out.println("Image was dropped at: " + event.getTarget().getClass());
                        if(db.hasImage()) {
                            if(ScreenImageView.class.equals(event.getTarget().getClass())) {
                                System.out.println("Dropped over image!");
                                ScreenImageView my_obj = ScreenImageView.class.cast(event.getTarget());
                                my_obj.setImage(db.getImage());
                                my_obj.setFitHeight(64);
                                my_obj.setFitWidth(64);
                                my_obj.setIndex(Integer.parseInt(db.getString()));
                                System.out.println("Index :" + db.getString() + " was received.");
                            }
                            event.setDropCompleted(true);
                        }
                        else {
                            event.setDropCompleted(false);
                        }
                        event.consume();
                    }
                });
                tempbox.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        if(collision_mode_) {
                            if(event.getButton() == MouseButton.PRIMARY) {
                                ScreenImageView my_obj = ScreenImageView.class.cast(event.getTarget());
                                my_obj.increaseCollision();
                            } else if (event.getButton() == MouseButton.SECONDARY) {
                                ScreenImageView my_obj = ScreenImageView.class.cast(event.getTarget());
                                my_obj.decreaseCollision();
                            }
                        }
                    }
                });
                screen_h_layout.getChildren().add(tempbox);
            }
            screen_v_layout.getChildren().add(screen_h_layout);
        }
        VBox main_layout = new VBox(8);

        HBox buttons = new HBox(4);
        Button btn_new = new Button("New screen");
        btn_new.setOnAction(e->newScreen());
        Button btn_open = new Button("Open screen");
        btn_open.setOnAction(e->openScreen());
        Button btn_save = new Button("Save screen");
        btn_save.setOnAction(e->saveScreen());
        Button btn_coll = new Button("Collision paint");
        btn_coll.setOnAction(e->collPaint());
        scroll_mode_ = new ComboBox<>();
        scroll_mode_.getItems().addAll("HORIZONTAL SCROLL","VERTICAL SCROLL");
        scroll_mode_.setValue("VERTICAL SCROLL");
        buttons.getChildren().addAll(btn_new,btn_open,btn_save,scroll_mode_,btn_coll);

        main_layout.getChildren().addAll(buttons,screen_v_layout);
        Scene scene = new Scene(main_layout);
        if(tilesize == 8) {
            setMinWidth(623);
            setMaxWidth(623);
            setMinHeight(603);
            setMaxHeight(603);
        }
        else if(tilesize == 16) {
            setMinWidth(575);
            setMaxWidth(575);
            setMinHeight(575);
            setMaxHeight(575);
        }
        else if(tilesize == 32) {
            setMinWidth(551);
            setMinHeight(541);
            setMaxWidth(551);
            setMaxHeight(541);
        }
        setScene(scene);
    }

    private void collPaint() {
        collision_mode_ = !collision_mode_;
        if(collision_mode_) {
            System.out.println("Collision mode on!");
            setTitle(getTitle() + " # COLLISION PAINT mode ON");
            for (ScreenImageView view :screen_imageviews_) {
                view.switchToCollisionMode();
                view.drawCollision();
            }
        }
        else {
            System.out.println("Collision mode off!");
            setTitle(getTitle().substring(0,getTitle().indexOf(".screen")+7));
            for (ScreenImageView view :screen_imageviews_) {
                view.unDrawCollision();
            }
        }
    }

    private void openScreen() {
        System.out.println("Open Screen data");
        FileChooser file_chooser = new FileChooser();
        file_chooser.setInitialDirectory(new File(PaletteColor.TEST_PATH));
        file_chooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Screen files","*.screen"));
        file_chooser.setTitle("Select used Screen data file");
        File file = file_chooser.showOpenDialog(this);
        if(file != null) {
            collision_mode_ = false;
            Path path = Paths.get(file.getAbsolutePath());
            screenFilename = path.getFileName().toString();
            setTitle(screenFilename);
            String file_extension = screenFilename.substring(screenFilename.lastIndexOf('.') + 1).toLowerCase();
            try {
                InputStream is = new FileInputStream(file);
                System.out.println("File size: " + file.length());
                if("VERTICAL SCROLL".equals(scroll_mode_.getValue().toString())) {
                    for (int i = 0; i < file.length(); ++i) {
                        byte[] data = new byte[1];
                        is.read(data,0,1);
                        MetaTile4x4 metatile4x4 = metatile4x4_listview_.getItems().get(data[0]);
                        int index = metatile4x4_listview_.getItems().indexOf(metatile4x4_listview_.getItems().get(data[0]));
                        screen_imageviews_.get(i).setImage(metatile4x4.getImage());
                        screen_imageviews_.get(i).setIndex(index);
                    }
                }
                else {
                    System.out.println("Tiedoston koko: " + file.length());
                    for (int i = 0; i < file.length(); ++i) {
                        byte[] data = new byte[1];
                        is.read(data,0,1);
                        MetaTile4x4 metatile4x4 = metatile4x4_listview_.getItems().get(data[0]);
                        int index = metatile4x4_listview_.getItems().indexOf(metatile4x4_listview_.getItems().get(data[0]));
                        int tmpX = i%6;
                        int tmpY = i/6;
                        // 0 = $00 0+0
                        // 1 = $08 8+0
                        // 2 = $10
                        // 3 = $18
                        // 4 = $20
                        // 5 = $28

                        // 6 = $01
                        // 7 = $09
                        // 8 = $11
                        // 9 = $19
                        // A = $21
                        // B = $29
                        screen_imageviews_.get(tmpX*8+tmpY).setImage(metatile4x4.getImage());
                        screen_imageviews_.get(tmpX*8+tmpY).setIndex(index);
                    }
                }

            } catch (IOException ex) {
                System.err.println("Could not read Screen data file.");
            }
        }
    }

    private void saveScreen() {
        System.out.println("Saving screen");

        FileChooser file_chooser = new FileChooser();
        file_chooser.setInitialDirectory(new File(PaletteColor.TEST_PATH));
        file_chooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Screen file","*.screen"));
        file_chooser.setTitle("Save screen bytes");
        file_chooser.setInitialFileName(screenFilename);
        File file = file_chooser.showSaveDialog(this);
        File file2 = new File(file.getAbsolutePath() + "_collision");
        if(file != null) {
            Path path = Paths.get(file.getAbsolutePath());
            String filename = path.getFileName().toString();
            String file_extension = filename.substring(filename.lastIndexOf('.') + 1).toLowerCase();
            setTitle(filename);
            int tiles_width = screen_width_/tilesize_;
            int tiles_height = screen_height_/tilesize_;

            byte[] screen_bytes = new byte[tiles_width*tiles_height];
            byte[] collision_bytes = new byte[tiles_height*2*2];

            if("VERTICAL SCROLL".equals(scroll_mode_.getValue().toString())) {
                for(int y = 0; y < screen_v_layout.getChildren().size(); ++y) {
                    HBox row_temp = ((HBox) screen_v_layout.getChildren().get(y));
                    byte temp_coll_byte_0 = 0;
                    byte temp_coll_byte_1 = 0;
                    byte temp_coll_byte_2 = 0;
                    byte temp_coll_byte_3 = 0;
                    for(int x = 0; x < row_temp.getChildren().size(); ++x) {
                        HBox temp_box = ((HBox) row_temp.getChildren().get(x));
                        ScreenImageView temp_image = ((ScreenImageView) temp_box.getChildren().get(0));
                        screen_bytes[y*tiles_width+x] = (byte)temp_image.getIndex().intValue();
                        if(x < 4) temp_coll_byte_0 += ((byte)temp_image.getCollision().intValue()&0x03)<<(6-(2*x));
                        else temp_coll_byte_1 += ((byte)temp_image.getCollision().intValue()&0x03)<<(6-(2*(x-4)));
                        if(x < 4) temp_coll_byte_2 += (((byte)temp_image.getCollision().intValue()&0x0C)>>2)<<(6-(2*x));
                        else temp_coll_byte_3 += (((byte)temp_image.getCollision().intValue()&0x0C)>>2)<<(6-(2*(x-4)));
                    }
                    System.out.println("Next 8 tiles");
                    System.out.println(Byte.toString(temp_coll_byte_0));
                    System.out.println(Byte.toString(temp_coll_byte_1));
                    System.out.println(Byte.toString(temp_coll_byte_2));
                    System.out.println(Byte.toString(temp_coll_byte_3));
                    collision_bytes[y*4] = temp_coll_byte_0;
                    collision_bytes[y*4+1] = temp_coll_byte_1;
                    collision_bytes[y*4+2] = temp_coll_byte_2;
                    collision_bytes[y*4+3] = temp_coll_byte_3;
                }
            }
            try {
                FileOutputStream fstream = new FileOutputStream(file);
                try {
                    fstream.write(screen_bytes);
                } finally {
                    fstream.close();
                }
                fstream = new FileOutputStream(file2);
                fstream.write(collision_bytes);
                fstream.close();
            } catch(IOException ex) {
                System.err.println("Could not write Screen data.");
            }
        }
    }
}
